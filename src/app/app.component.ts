import { NestedTreeControl } from '@angular/cdk/tree';
import { Component, Injectable, ViewChild } from '@angular/core';
import { MatTreeModule, MatTreeNestedDataSource } from '@angular/material/tree';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';


/**
 * Json node data with nested structure. Each node has a name and a value or a list of children
 */
export enum TypeConstants {
  element = "element", attribute = "attribute"
}

/**
 * Json node data with nested structure. Each node has a name and a value or a list of children
 */
export enum CardinalityConstants {
	zeroOrMore = "zeroOrMore", onOrMore = "onOrMore", optional = "optional", obligatoire = "obligatoire" 
}

/**
 * Json node data with nested structure. Each node has a name and a value or a list of children
 */
export enum DataTypeConstants {
	string = "string", dateTime = "dateTime", date = "date", ID = "ID", anyURI = "anyURI" , token = "token", undefined = "undefined" 
}

/**
 * Json node data with nested structure. Each node has a name and a value or a list of children
 */
export enum ValueOrDataConstants {
	value = "value", data = "data", nsName = "nsName", undefined = "undefined"
}

export class FileNode {
  id: number;
  parentId: number;
  name: string;
  valueOrData: ValueOrDataConstants;
  value: string;
  type: TypeConstants;
  dataType: DataTypeConstants;
  cardinality: CardinalityConstants;
  level: number;
  children: FileNode[];
}

/**
 * The Json tree data in string. The data could be parsed into Json object
 */
const TREE_DATA = {
//  Location: {
//    level1_sl1: {
//      level2_sl1: {
//        compiler: 'ts',
//        core: 'ts'
//      }
//    },
//    level1_sl2: {
//      level2_sl1: {
//        button: 'ts',
//        checkbox: 'ts',
//        input: 'ts'
//      }
//    }
//  }
}


/**
 * File database, it can build a tree structured Json object from string.
 * Each node in Json object represents a file or a directory. For a file, it has name and type.
 * For a directory, it has name and children (a list of files or directories).
 * The input will be a json object string, and the output is a list of `FileNode` with nested
 * structure.
 */

//@Injectable()
//export class DataService {
//  constructor(protected http: HttpClient) {}
//
////  public getFileNodes(): Observable<FileNode[]> {
////    return this.http.get('http://localhost:3000/articles/').pipe(map((jsonArray: Object[]) => jsonArray.map(jsonItem => jsonItem)));
////  }
//  
//  getData() {
//	    this.http.get('https://api.fakeservice.com/datas').subscribe(res => {
//	        console.log(res)
//	    });
//	}
//}

@Injectable()
export class FileDatabase {

  dataChange = new BehaviorSubject<FileNode[]>([]);

  get data(): FileNode[] { return this.dataChange.value; }

  parentNodeMap = new Map<FileNode, FileNode>();

  //protected http: HttpClient
  constructor() {
    this.initialize();
  }
 

  treeData = [{"name":"ArchiveTransfer","type":"element","cardinality":"obligatoire","valueOrData":"undefined","dataType":"undefined","value":"undefined","level":0,"id":0,"parentId":null,"children":
	  [{"name":"id","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"ID","value":"undefined","level":1,"id":1,"parentId":0,"children":[]},
	  {"name":"Comment","type":"element","cardinality":"obligatoire","valueOrData":"data","dataType":"string","value":"undefined","level":1,"id":2,"parentId":0,"children":[]},
	  {"name":"Date","type":"element","cardinality":"obligatoire","valueOrData":"data","dataType":"dateTime","value":"undefined","level":1,"id":3,"parentId":0,"children":[]},
	  {"name":"MessageIdentifier","type":"element","cardinality":"obligatoire","valueOrData":"data","dataType":"token","value":"undefined","level":1,"id":4,"parentId":0,"children":
		  [{"name":"schemeDataURI","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"anyURI","value":"undefined","level":2,"id":5,"parentId":4,"children":[]},
		  {"name":"schemeID","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"token","value":"undefined","level":2,"id":6,"parentId":4,"children":[]},
		  {"name":"schemeAgencyName","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"string","value":"undefined","level":2,"id":7,"parentId":4,"children":[]},
		  {"name":"schemeAgencyID","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"token","value":"undefined","level":2,"id":8,"parentId":4,"children":[]},
		  {"name":"schemeName","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"string","value":"undefined","level":2,"id":9,"parentId":4,"children":[]},
		  {"name":"schemeVersionID","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"token","value":"undefined","level":2,"id":10,"parentId":4,"children":[]},
		  {"name":"schemeURI","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"anyURI","value":"undefined","level":2,"id":11,"parentId":4,"children":[]}]},
	  {"name":"ArchivalAgreement","type":"element","cardinality":"obligatoire","valueOrData":"data","dataType":"token","value":"undefined","level":1,"id":12,"parentId":0,"children":
		  [{"name":"schemeDataURI","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"anyURI","value":"undefined","level":2,"id":13,"parentId":12,"children":[]},
		  {"name":"schemeID","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"token","value":"undefined","level":2,"id":14,"parentId":12,"children":[]},
		  {"name":"schemeAgencyName","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"string","value":"undefined","level":2,"id":15,"parentId":12,"children":[]},
		  {"name":"schemeAgencyID","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"token","value":"undefined","level":2,"id":16,"parentId":12,"children":[]},
		  {"name":"schemeName","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"string","value":"undefined","level":2,"id":17,"parentId":12,"children":[]},
		  {"name":"schemeVersionID","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"token","value":"undefined","level":2,"id":18,"parentId":12,"children":[]},
		  {"name":"schemeURI","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"anyURI","value":"undefined","level":2,"id":19,"parentId":12,"children":[]}]},
	  {"name":"CodeListVersions","type":"element","cardinality":"obligatoire","valueOrData":"undefined","dataType":"undefined","value":"undefined","level":1,"id":20,"parentId":0,"children":[
		  {"name":"id","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"ID","value":"undefined","level":2,"id":21,"parentId":20,"children":[]},
		  {"name":"ReplyCodeListVersion","type":"element","cardinality":"optional","valueOrData":"value","dataType":"token","value":"ReplyCodeListVersion0","level":2,"id":22,"parentId":20,"children":[
			  {"name":"listName","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"string","value":"undefined","level":3,"id":23,"parentId":22,"children":[]},
			  {"name":"listAgencyID","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"token","value":"undefined","level":3,"id":24,"parentId":22,"children":[]},
			  {"name":"listSchemeURI","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"anyURI","value":"undefined","level":3,"id":25,"parentId":22,"children":[]},
			  {"name":"listID","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"token","value":"undefined","level":3,"id":26,"parentId":22,"children":[]},
			  {"name":"listAgencyName","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"string","value":"undefined","level":3,"id":27,"parentId":22,"children":[]},
			  {"name":"listURI","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"anyURI","value":"undefined","level":3,"id":28,"parentId":22,"children":[]},
			  {"name":"listVersionID","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"token","value":"undefined","level":3,"id":29,"parentId":22,"children":[]}]},
		  {"name":"MessageDigestAlgorithmCodeListVersion","type":"element","cardinality":"optional","valueOrData":"value","dataType":"token","value":"MessageDigestAlgorithmCodeListVersion0","level":2,"id":30,"parentId":20,"children":[
			  {"name":"listName","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"string","value":"undefined","level":3,"id":31,"parentId":30,"children":[]},
			  {"name":"listAgencyID","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"token","value":"undefined","level":3,"id":32,"parentId":30,"children":[]},
			  {"name":"listSchemeURI","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"anyURI","value":"undefined","level":3,"id":33,"parentId":30,"children":[]},
			  {"name":"listID","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"token","value":"undefined","level":3,"id":34,"parentId":30,"children":[]},
			  {"name":"listAgencyName","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"string","value":"undefined","level":3,"id":35,"parentId":30,"children":[]},
			  {"name":"listURI","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"anyURI","value":"undefined","level":3,"id":36,"parentId":30,"children":[]},
			  {"name":"listVersionID","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"token","value":"undefined","level":3,"id":37,"parentId":30,"children":[]}]},
		  {"name":"RelationshipCodeListVersion","type":"element","cardinality":"optional","valueOrData":"value","dataType":"token","value":"RelationshipCodeListVersion0","level":2,"id":134,"parentId":20,"children":[
			  {"name":"listName","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"string","value":"undefined","level":3,"id":135,"parentId":134,"children":[]},
			  {"name":"listAgencyID","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"token","value":"undefined","level":3,"id":136,"parentId":134,"children":[]},
			  {"name":"listSchemeURI","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"anyURI","value":"undefined","level":3,"id":137,"parentId":134,"children":[]},
			  {"name":"listID","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"token","value":"undefined","level":3,"id":138,"parentId":134,"children":[]},
			  {"name":"listAgencyName","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"string","value":"undefined","level":3,"id":139,"parentId":134,"children":[]},
			  {"name":"listURI","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"anyURI","value":"undefined","level":3,"id":140,"parentId":134,"children":[]},
			  {"name":"listVersionID","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"token","value":"undefined","level":3,"id":141,"parentId":134,"children":[]}]}]},
	  {"name":"DataObjectPackage","type":"element","cardinality":"obligatoire","valueOrData":"undefined","dataType":"undefined","value":"undefined","level":1,"id":142,"parentId":0,"children":[
		  {"name":"xml:id","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"ID","value":"undefined","level":2,"id":143,"parentId":142,"children":[]},
		  {"name":"DescriptiveMetadata","type":"element","cardinality":"obligatoire","valueOrData":"undefined","dataType":"undefined","value":"undefined","level":2,"id":144,"parentId":142,"children":[
			  {"name":"ArchiveUnit","type":"element","cardinality":"zeroOrMore","valueOrData":"undefined","dataType":"undefined","value":"undefined","level":3,"id":145,"parentId":144,"children":[
				  {"name":"id","type":"attribute","cardinality":"obligatoire","valueOrData":"data","dataType":"ID","value":"undefined","level":4,"id":146,"parentId":145,"children":[]},
				  {"name":"ArchiveUnitProfile","type":"element","cardinality":"optional","valueOrData":"data","dataType":"token","value":"undefined","level":4,"id":147,"parentId":145,"children":[]},
				  {"name":"Management","type":"element","cardinality":"optional","valueOrData":"undefined","dataType":"undefined","value":"undefined","level":4,"id":148,"parentId":145,"children":[]},
				  {"name":"Content","type":"element","cardinality":"optional","valueOrData":"undefined","dataType":"undefined","value":"undefined","level":4,"id":149,"parentId":145,"children":[]},
				  {"name":"ArchiveUnit","type":"element","cardinality":"zeroOrMore","valueOrData":"undefined","dataType":"undefined","value":"undefined","level":4,"id":150,"parentId":145,"children":[
					  {"name":"id","type":"attribute","cardinality":"obligatoire","valueOrData":"data","dataType":"ID","value":"undefined","level":5,"id":151,"parentId":150,"children":[]},
					  {"name":"ArchiveUnitRefId","type":"element","cardinality":"obligatoire","valueOrData":"data","dataType":"NCName","value":"undefined","level":5,"id":152,"parentId":150,"children":[]}]}]},
			  {"name":"ArchiveUnit","type":"element","cardinality":"zeroOrMore","valueOrData":"undefined","dataType":"undefined","value":"undefined","level":3,"id":153,"parentId":144,"children":[
				  {"name":"id","type":"attribute","cardinality":"obligatoire","valueOrData":"data","dataType":"ID","value":"undefined","level":4,"id":154,"parentId":153,"children":[]},
				  {"name":"ArchiveUnitProfile","type":"element","cardinality":"optional","valueOrData":"data","dataType":"token","value":"undefined","level":4,"id":155,"parentId":153,"children":[]},
				  {"name":"Management","type":"element","cardinality":"optional","valueOrData":"undefined","dataType":"undefined","value":"undefined","level":4,"id":156,"parentId":153,"children":[]},
				  {"name":"Content","type":"element","cardinality":"optional","valueOrData":"undefined","dataType":"undefined","value":"undefined","level":4,"id":157,"parentId":153,"children":[]},
				  {"name":"ArchiveUnit","type":"element","cardinality":"zeroOrMore","valueOrData":"undefined","dataType":"undefined","value":"undefined","level":4,"id":158,"parentId":153,"children":[
					  {"name":"id","type":"attribute","cardinality":"obligatoire","valueOrData":"data","dataType":"ID","value":"undefined","level":5,"id":159,"parentId":158,"children":[]},
					  {"name":"ArchiveUnitRefId","type":"element","cardinality":"obligatoire","valueOrData":"data","dataType":"NCName","value":"undefined","level":5,"id":160,"parentId":158,"children":[]}]}]}]},
		  {"name":"ManagementMetadata","type":"element","cardinality":"obligatoire","valueOrData":"undefined","dataType":"undefined","value":"undefined","level":2,"id":161,"parentId":142,"children":[
			  {"name":"id","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"ID","value":"undefined","level":3,"id":162,"parentId":161,"children":[]},
			  {"name":"ArchivalProfile","type":"element","cardinality":"optional","valueOrData":"data","dataType":"token","value":"undefined","level":3,"id":163,"parentId":161,"children":[]},
			  {"name":"OriginatingAgencyIdentifier","type":"element","cardinality":"obligatoire","valueOrData":"value","dataType":null,"value":"Service_producteur","level":3,"id":164,"parentId":161,"children":[]},
			  {"name":"SubmissionAgencyIdentifier","type":"element","cardinality":"obligatoire","valueOrData":"value","dataType":null,"value":"Service_versant","level":3,"id":165,"parentId":161,"children":[]}]}]},
	  {"name":"ArchivalAgency","type":"element","cardinality":"obligatoire","valueOrData":"undefined","dataType":"undefined","value":"undefined","level":1,"id":166,"parentId":0,"children":[
		  {"name":"Identifier","type":"element","cardinality":"obligatoire","valueOrData":"value","dataType":null,"value":"Identifier4","level":2,"id":167,"parentId":166,"children":[]}]},
	  {"name":"TransferringAgency","type":"element","cardinality":"obligatoire","valueOrData":"undefined","dataType":"undefined","value":"undefined","level":1,"id":168,"parentId":0,"children":[
		  {"name":"Identifier","type":"element","cardinality":"obligatoire","valueOrData":"value","dataType":null,"value":"Identifier5","level":2,"id":169,"parentId":168,"children":[]}]}]}	

  ] as FileNode[];
  

//  treeData = [{"name":"ArchiveTransfer","type":"element","cardinality":"obligatoire","valueOrData":"undefined","dataType":"undefined","value":"undefined","level":0,"id":0,"parentId":null,"children"
//	  :[{"name":"undefined","type":"attribute","cardinality":"zeroOrMore","valueOrData":"nsName","dataType":"undefined","value":"undefined","level":1,"id":1,"parentId":0,"children":[]}
//	  ,{"name":"id","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"ID","value":"undefined","level":1,"id":2,"parentId":0,"children":[]}
//	  ,{"name":"Comment","type":"element","cardinality":"obligatoire","valueOrData":"data","dataType":"string","value":"undefined","level":1,"id":3,"parentId":0,"children":[]}
//	  ,{"name":"Date","type":"element","cardinality":"obligatoire","valueOrData":"data","dataType":"dateTime","value":"undefined","level":1,"id":4,"parentId":0,"children":[]}
//	  ,{"name":"MessageIdentifier","type":"element","cardinality":"obligatoire","valueOrData":"data","dataType":"token","value":"undefined","level":1,"id":5,"parentId":0,"children"
//		  :[{"name":"schemeDataURI","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"anyURI","value":"undefined","level":2,"id":6,"parentId":5,"children":[]}
//			,{"name":"schemeID","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"token","value":"undefined","level":2,"id":7,"parentId":5,"children":[]}
//		  ,{"name":"schemeAgencyName","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"string","value":"undefined","level":2,"id":8,"parentId":5,"children":[]}]},
//		  {"name":"CodeListVersions","type":"element","cardinality":"obligatoire","valueOrData":"undefined","dataType":"undefined","value":"undefined","level":1,"id":9,"parentId":0,"children"
//			  :[{"name":"id","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"ID","value":"undefined","level":2,"id":10,"parentId":9,"children":[]},
//				  {"name":"StorageRuleCodeListVersion","type":"element","cardinality":"optional","valueOrData":"value","dataType":"token","value":" StorageRuleCodeListVersion0 ","level":2,"id":11,"parentId":9,"children"
//				  :[{"name":"listName","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"string","value":"undefined","level":3,"id":12,"parentId":11,"children":[]}
//				  ,{"name":"listAgencyID","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"token","value":"undefined","level":3,"id":13,"parentId":11,"children":[]}
//				  ,{"name":"listSchemeURI","type":"attribute","cardinality":"optional","valueOrData":"data","dataType":"anyURI","value":"undefined","level":3,"id":14,"parentId":11,"children":[]}]}]}
//		  ,{"name":"ArchivalAgency","type":"element","cardinality":"obligatoire","valueOrData":"undefined","dataType":"undefined","value":"undefined","level":1,"id":15,"parentId":0,"children"
//			  :[{"name":"Identifier","type":"element","cardinality":"obligatoire","valueOrData":"value","dataType":"undefined","value":"CINES","level":2,"id":16,"parentId":15,"children":[]}]},
//			  {"name":"TransferringAgency","type":"element","cardinality":"obligatoire","valueOrData":"undefined","dataType":"undefined","value":"undefined","level":1,"id":17,"parentId":0,"children"
//				  :[{"name":"Identifier","type":"element","cardinality":"obligatoire","valueOrData":"undefined","dataType":"undefined","value":"undefined","level":2,"id":18,"parentId":17,"children":[]}]}]
//	  
//  }
//  ] as FileNode[];
  
  initialize() {
	
//	  this.data.getFileNodes().subscribe(
//	fileNodes => this.fileNodes = fileNodes
//    );
	  
	  
    // Parse the string to json object.
    const dataObject = TREE_DATA;
    const fakeDataObjecct = { Location: {} }

    // Build the tree nodes from Json object. The result is a list of `FileNode` with nested
    //     file node as children.
    // input as array

    //const data = this.buildFileTree(fakeDataObjecct, 0);    

    const data = this.treeData;
    console.log(this.data);

    //this.populateParentMap(this.treeData);

    // Notify the change.
    this.dataChange.next(data);
  }



  /**
   * Build the file structure tree. The `value` is the Json object, or a sub-tree of a Json object.
   * The return value is the list of `FileNode`.
   */
  buildFileTree(obj: object, level: number): FileNode[] {
    // @pankaj This should recive Root node of Tree of Type FileNode
    // so we dont have to create a new node and use it as it is
    //console.log(obj);
    return Object.keys(obj).reduce<FileNode[]>((accumulator, key) => {
      // console.log(key);
      const value = obj[key];
      const node = new FileNode();
      node.id = level;

      node.level = level;
      node.name = key;
      node.parentId = null;
      if (value != null) {
        if (typeof value === 'object') {
          node.children = this.buildFileTree(value, level + 1);
        } else {
          node.type = value;
        }
      }

      return accumulator.concat(node);
    }, []);
  }

  /** Add an item Tree node */
  public insertItem(parent: FileNode, name: string) {
    if (parent.children) {
      //console.log("insert ")

      //if (parent.type !== 'SECTION') {
        let newNode: FileNode;
        newNode = new FileNode();
        newNode.name = name;
        newNode.children = [];
        newNode.level = parent.level + 1;
        console.log(newNode.level);
        newNode.parentId = parent.id;
        newNode.id = newNode.level + ((parent.children.length + 1) / 10.0);


        console.log(parent.children.length);
        console.log(newNode.id);

        parent.children.push(newNode);
        this.parentNodeMap.set(newNode, parent);
        //console.log(newNode);

      //} else {
        console.log("No More Nodes can be inserted");
      //}
      //this.dataChange.next(this.data);
    }

  }
  public removeItem(currentNode: FileNode, root: FileNode) {
    //const parentNode = this.parentNodeMap.get(currentNode);
    const parentNode = this.findParent(currentNode.parentId, root);
    console.log("parentNode " + JSON.stringify(parentNode))
    const index = parentNode.children.indexOf(currentNode);
    if (index !== -1) {
      parentNode.children.splice(index, 1);
      this.dataChange.next(this.data);
      this.parentNodeMap.delete(currentNode);
    }
    console.log("currentNode" + index);

  }
  updateItem(node: FileNode, name: string) {
    node.name = name;
    this.dataChange.next(this.data);
  }
  public findParent(id: number, node: any): any {

    console.log("id " + id + " node" + node.id);
    if (node != undefined && node.id === id) {
      return node;
    } else {
      console.log("ELSE " + JSON.stringify(node.children));
      for (let element in node.children) {
        console.log("Recursive " + JSON.stringify(node.children[element].children));
        if (node.children[element].children != undefined && node.children[element].children.length > 0) {
          return this.findParent(id, node.children[element]);
        } else {
          continue;
        }
      }
    }
  }
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [FileDatabase]
})
export class AppComponent {
    @ViewChild('treeSelector', {static: true}) tree: any;

  nestedTreeControl: NestedTreeControl<FileNode>;
  nestedDataSource: MatTreeNestedDataSource<FileNode>;

  //CardinalityConstants/dataTypeConstants/ValueOrDataConstants/typeConstants
  
  typeType = Object.keys(TypeConstants);
  valueOrDataType = Object.keys(ValueOrDataConstants);
  dataTypeType = Object.keys(DataTypeConstants);
  
  cardinalityType = Object.keys(CardinalityConstants);
  


  constructor(public database: FileDatabase /*, private httpww: HttpClient*/) {
    this.nestedTreeControl = new NestedTreeControl<FileNode>(this._getChildren);
    this.nestedDataSource = new MatTreeNestedDataSource();

    database.dataChange.subscribe(data => {this.nestedDataSource.data = data
    		this.nestedTreeControl.dataNodes = data;
  	});
    
//    this.http.get('https://api.fakeservice.com/datas').subscribe(res => {
//        console.log(res)
//    });

  }
  
//  this.data.getFileNodes().subscribe(
//	fileNodes => this.fileNodes = fileNodes
//  );

  hasNestedChild = (_: number, nodeData: FileNode) => !nodeData.type;

  private _getChildren = (node: FileNode) => node.children;
  /** Select the category so we can insert the new item. */
  addNewItem(node: FileNode) {
    this.database.insertItem(node, '');
    //this.tree.renderNodeChanges(this.database.data);
    this.nestedTreeControl.expand(node);
    //console.log(this.nestedTreeControl);

    this.renderChanges()
    this.getTree();
  }

  public remove(node: FileNode) {
    console.log("currentNode");

    this.database.removeItem(node, this.database.data[0]);
    this.renderChanges()
    this.getTree();

  }

  renderChanges() {
    let data = this.nestedDataSource.data;
    this.nestedDataSource.data = null;
    this.nestedDataSource.data = data;

  }

  getTree() {
    console.log(JSON.stringify(this.database.data));


  }

  check() {
    console.log("parent ", JSON.stringify(this.database.findParent(1.1, this.database.data[0])));
  }

  public getArr(node: FileNode) {
    let arr = [];
//    if (node.level === 0)
//      arr = [LocationConstants.WAREHOUSE];
//
//    else {
//      // console.log("locarr" + this.locationType);
//
//      arr = this.locationType.slice(node.level)
//      //console.log("arr");
//
//
//    }
//    console.log(arr);
    return arr;
  }

}
